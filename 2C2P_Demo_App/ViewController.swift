//
//  ViewController.swift
//  2C2P_Demo_App
//
//  Created by Macbook Pro on 03/05/2019.
//  Copyright © 2019 Macbook Pro. All rights reserved.
//

import UIKit
import PGW

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func DS(_ sender: Any) {
        
        let paymentMethod:CreditCard3DS = CreditCard3DS(viewController: self)
        let CreditCardinfo:CreditCardPayment = getCCwithToken()
        paymentMethod.setCardPayment(card: CreditCardinfo )
        
        let payment = PaymentTemp.init()
        payment.payment_desc = "3DS Fund In 1 Baht...."
        payment.payment_amount =  Utils_2c2p.convertTo_12_Digit(aData: "1.00");
        payment.payment_currencyType = "THB"
        payment.payment_productCode = "A01B23RGH"
        
        
        payment.payment_user_define_1 = "TEST1"
        payment.payment_user_define_2 = "TEST2"
        payment.payment_user_define_3 = "TEST3"
        payment.payment_user_define_4 = "TEST4"
        payment.payment_user_define_5 = "TEST5"
        
        
        
        paymentMethod.execute(aPayment: payment)
        
    }
    
    @IBAction func CCWithToken(_ sender: Any) {
         
        if CCToken.share.ccToken != nil {
            
            let paymentMethod:CreditCardWithToken = CreditCardWithToken(viewController: self)
            paymentMethod.setCardToken(token: CCToken.share.ccToken , secret: "111")
            
            let payment = PaymentTemp.init()
            payment.payment_desc = "CCWithToken Fund In 1 Baht...."
            payment.payment_amount = Utils_2c2p.convertTo_12_Digit(aData: "1.00");
            payment.payment_currencyType = "THB"
            payment.payment_productCode = "A01B23RGH"
    
            paymentMethod.execute(aPayment: payment)
            
        }else{
            
            let alert = UIAlertController(title: "Error" , message: "No CCToken" , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true);
            
        }
    }
    
    @IBAction func InstallPlan(_ sender: Any) {
        
        let paymentMethod:InstallmentPaymentPlan = InstallmentPaymentPlan(viewController: self)
        let CreditCardinfo:CreditCardPayment = getCC1()
        paymentMethod.setCardPayment(card: CreditCardinfo )
        
        let payment = PaymentTemp.init()
        payment.payment_desc = "InstallPlan Fund In 1 Baht...."
        payment.payment_amount =  Utils_2c2p.convertTo_12_Digit(aData: "5000.00"); // Min-Total
        payment.payment_currencyType = "THB"
        payment.payment_productCode = "A01B23RGH"
        
        payment.payment_Installment_Period = 10
        
        paymentMethod.execute(aPayment: payment)
        
    }
    
    @IBAction func Recurring(_ sender: Any) {
    
        let paymentMethod:RecurringPaymentPlan = RecurringPaymentPlan(viewController: self)
        let CreditCardinfo:CreditCardPayment = getCC1()
        paymentMethod.setCardPayment(card: CreditCardinfo )
        
        let payment = PaymentTemp.init()
        payment.payment_desc = "Recurring Fund In 1 Baht...."
        payment.payment_amount =  Utils_2c2p.convertTo_12_Digit(aData: "1.00");
        payment.payment_currencyType = "THB"
        payment.payment_productCode = "A01B23RGH"
        
        payment.payment_recurring = "Y"
        payment.payment_invoice_prefix = String(Int(Date().timeIntervalSince1970))
        payment.payment_recurring_amount = Utils_2c2p.convertTo_12_Digit(aData: "1.50");
        payment.payment_allow_accumulate = "Y"
        payment.payment_max_accumulateAmt = Utils_2c2p.convertTo_12_Digit(aData: "10.00");
        payment.payment_recurring_interval = "5"
        payment.payment_recurring_count = "3"
        payment.payment_charge_next_date = paymentMethod.getChargeNextDate()
        
        paymentMethod.execute(aPayment: payment)
        
    }
    
    // CC info
    func getCC1() -> CreditCardPayment{
        
        let creditCardPayment:CreditCardPayment = CreditCardPaymentBuilder(pan: "1111111111111111")
            .expiryMonth(11)
            .expiryYear(1111111111111111)
            .securityCode("111")
            .storeCard(false)
            .build()
        
        return creditCardPayment;
        
    }
    
    func getCCwithToken() -> CreditCardPayment{
        
        let creditCardPayment:CreditCardPayment = CreditCardPaymentBuilder(pan: "1111111111111111")
            .expiryMonth(11)
            .expiryYear(1111111111111111)
            .securityCode("111")
            .storeCard(true)
            .build()
        
        return creditCardPayment;
        
    }
    
    //============================================= Not Use Now
    @IBAction func No3DS(_ sender: Any) {
        
        let paymentMethod:CreditCardNon3DS = CreditCardNon3DS(viewController: self)
        let CreditCardinfo:CreditCardPayment = getCC1()
        paymentMethod.setCardPayment(card: CreditCardinfo )
        
        let payment = PaymentTemp.init()
        payment.payment_desc = "No3DS Fund In 1 Baht...."
        payment.payment_amount = Utils_2c2p.convertTo_12_Digit(aData: "1.00");
        payment.payment_currencyType = "THB"
        payment.payment_productCode = "A01B23RGH"
        
        paymentMethod.execute(aPayment: payment)
        
    }
    
    @IBAction func CCTokein(_ sender: Any) {
        
        let paymentMethod:CreditCardTokenization = CreditCardTokenization(viewController: self)
        let CreditCardinfo:CreditCardPayment = getCC1()
        paymentMethod.setCardPayment(card: CreditCardinfo )
        
        let payment = PaymentTemp.init()
        payment.payment_desc = "CCTokein Fund In 1 Baht...."
        payment.payment_amount = Utils_2c2p.convertTo_12_Digit(aData: "1.00");
        payment.payment_currencyType = "THB"
        payment.payment_productCode = "A01B23RGH"
        
        paymentMethod.execute(aPayment: payment)
        
    }
    
}




 
