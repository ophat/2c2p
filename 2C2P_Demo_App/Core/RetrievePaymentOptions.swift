//
//  RetrievePaymentOptions.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 6/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation
import PGW

class RetrievePaymentOptions: BasePayment {
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-payment-options-api
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#section--paymentoptionrequest-class-parameters-
     */
    
    override func buildPaymentToken() -> PaymentTokenRequest {
        
        //Construct payment token request
        let request:PaymentTokenRequest = PaymentTokenRequest()
        request.paymentChannel = "ALL"
        
        return request
    }
    
    func execute(aPayment:PaymentTemp) {
        if CKey.isReady {
            self.progressDialog.message = Constants.common_message_payment_token_message
            self.progressDialog.showLoadingIndicator()
            
            //Step 1 : Get payment token
            self.merchantServerSimulator.getPaymentToken(paymentTokenRequest: self.buildPaymentToken(),payment:aPayment, success: { (response:String) in
                
                self.progressDialog.hideLoadingIndicator()
                
                //Submit payment
                self.submit(paymentToken: self.stringHelper.extractPaymentToken(response: response))
            }) { (error:NSError) in
                
                self.progressDialog.hideLoadingIndicator()
                
                print("\(type(of: self)) \(error.domain)")
            }
        }
    }
    
    
    override func submit(paymentToken: String) {
        
        self.progressDialog.message = Constants.common_message_payment_processing_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 2: Construct payment option request.
        let paymentOptionRequest:PaymentOptionRequest = PaymentOptionRequest()
        paymentOptionRequest.paymentToken = paymentToken
        
        //Step 3: Retrieve payment options.
        PGWSDK.shared.paymentOption(paymentOptionRequest: paymentOptionRequest, success: { (response:PaymentOptionResponse) in
            
            self.progressDialog.hideLoadingIndicator()
            
            if response.responseCode == APIResponseCode.API_SUCCESS {
                
                let channels:[Channel] = response.channels
                
                for channel:Channel in channels {
                    
                    let name:String = channel.name
                    let iconUrl:String = channel.iconUrl
                    let paymentChannel:String = channel.paymentChannel()
                    
                    print("\(type(of: self)) \(String(describing: "Enabled Payment channel")) :: \(String(describing: name))")
                }
            } else {
                
                //Get error response and display error
                print("\(type(of: self)) \(String(describing: response.responseCode)) :: \(String(describing: response.responseDescription))")
            }
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
        
    }
    
    override func inquiry(transactionID: String) {
        //no required for this API
    }
}
