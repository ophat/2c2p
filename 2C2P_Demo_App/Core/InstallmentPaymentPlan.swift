//
//  InstallmentPaymentPlan.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 6/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation
import PGW

class InstallmentPaymentPlan: BasePayment {
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-installment-payment-plan
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#installment-payment-builder
     */
    var creditCardPayment:CreditCardPayment?
    
    override func buildPaymentToken() -> PaymentTokenRequest {
        
        //Construct payment token request
        let request:PaymentTokenRequest = PaymentTokenRequest()
        request.request3DS = "Y"
        
        //Enable IPP payment option
        request.paymentChannel = "IPP"
        
        //Set advance payment option
        request.interestType = "M"   //Paid by M = merchant / C = customer.
        
        return request
    }
    
    
    func execute(aPayment:PaymentTemp) {
        if CKey.isReady {
            
            CKey.initPGW_Installment()
            
            self.progressDialog.message = Constants.common_message_payment_token_message
            self.progressDialog.showLoadingIndicator()
            
            //Step 1 : Get payment token
            self.merchantServerSimulator.getPaymentToken(paymentTokenRequest: self.buildPaymentToken(),payment:aPayment, success: { (response:String) in
                
                self.progressDialog.hideLoadingIndicator()
                
                //Submit payment
                self.submit(paymentToken: response , aPeriod: aPayment.payment_Installment_Period)
                //self.submit(paymentToken: self.stringHelper.extractPaymentToken(response: response))
            }) { (error:NSError) in
                
                self.progressDialog.hideLoadingIndicator()
                
                print("\(type(of: self)) \(error.domain)")
            }
        }
    }
    
    
    func submit(paymentToken: String , aPeriod : Int) {
        
        print("aPeriod",aPeriod)
        
        self.progressDialog.message = Constants.common_message_payment_processing_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 3: Construct installment request.
        let installmentPayment:InstallmentPayment = InstallmentPaymentBuilder(period: aPeriod)
                                                    .withCreditCardPayment(creditCardPayment!)
                                                    .build()
        
        //Step 4: Construct transaction request.
        let transactionRequest:TransactionRequest = TransactionRequestBuilder(paymentToken: paymentToken)
                                                    .withInstallmentPayment(installmentPayment)
                                                    .build()
        
        //Step 5: Execute payment request.
        PGWSDK.shared.proceedTransaction(transactionRequest: transactionRequest, success: { (response:TransactionResultResponse) in
            
            self.progressDialog.hideLoadingIndicator()
            
            if response.responseCode == APIResponseCode.TRANSACTION_COMPLETED {
                
                //Inquiry payment result by using transaction id.
                self.inquiry(transactionID: response.transactionID!)
            }
            else if response.responseCode == APIResponseCode.TRANSACTION_AUTHENTICATE {
                
                guard let redirectUrl:String = response.redirectUrl else { return }
                self.openAuthentication(redirectUrl: redirectUrl) //Open WebView for 3DS
            }
            else {
                
                //Get error response and display error
                print("\(type(of: self)) \(String(describing: response.responseCode)) :: \(String(describing: response.responseDescription))")
            }
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    override func inquiry(transactionID: String) {
        
        self.progressDialog.message = Constants.common_message_payment_result_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 5: Get payment result.
        self.merchantServerSimulator.inquiryPaymentResult(paymentInquiryRequest: self.buildPaymentInquiry(transactionID: transactionID), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            self.displayResult(response)
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    func setCardPayment( card:CreditCardPayment ){
        creditCardPayment = card
    }

    private func openAuthentication(redirectUrl:String) {
        
        let wkWebVC:WKWebViewController =  self.viewController.storyboard?.instantiateViewController(withIdentifier: "webview") as! WKWebViewController
        wkWebVC.redirectUrl = redirectUrl
        
        self.viewController.present(wkWebVC, animated: true, completion: nil)
        
    }
    
    
}
