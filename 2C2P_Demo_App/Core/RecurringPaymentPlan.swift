//
//  RecurringPaymentPlan.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 6/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation
import PGW

class RecurringPaymentPlan: BasePayment {
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-recurring-payment-plan
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#credit-card-payment-builder
     */
    var creditCardPayment:CreditCardPayment?
    
    override func buildPaymentToken() -> PaymentTokenRequest {
        
        //Construct payment token request
        let request:PaymentTokenRequest = PaymentTokenRequest()
        request.paymentChannel = "CC"
        request.request3DS = "Y"
        
        return request
    }
    
    
    func execute(aPayment:PaymentTemp) {
        
        if CKey.isReady {
            
            CKey.initPGW()
            
            self.progressDialog.message = Constants.common_message_payment_token_message
            self.progressDialog.showLoadingIndicator()
            
            //Step 1 : Get payment token
            self.merchantServerSimulator.getPaymentToken(paymentTokenRequest: self.buildPaymentToken(),payment:aPayment, success: { (response:String) in
                
                self.progressDialog.hideLoadingIndicator()
                
                //Submit payment
                self.submit(paymentToken: response)
                //self.submit(paymentToken: self.stringHelper.extractPaymentToken(response: response))
            }) { (error:NSError) in
                
                self.progressDialog.hideLoadingIndicator()
                
                print("\(type(of: self)) \(error.domain)")
            }
        }
    }
    
    
    override func submit(paymentToken: String) {
        
        self.progressDialog.message = Constants.common_message_payment_processing_message
        self.progressDialog.showLoadingIndicator()
        
        
        //Step 3: Construct transaction request.
        let transactionRequest:TransactionRequest = TransactionRequestBuilder(paymentToken: paymentToken)
            .withCreditCardPayment(creditCardPayment!)
                                                    .build()
        
        //Step 4: Execute payment request.
        PGWSDK.shared.proceedTransaction(transactionRequest: transactionRequest, success: { (response:TransactionResultResponse) in
            
            self.progressDialog.hideLoadingIndicator()
            
            if response.responseCode == APIResponseCode.TRANSACTION_COMPLETED {
                
                //Inquiry payment result by using transaction id.
                self.inquiry(transactionID: response.transactionID!)
            }
            else if response.responseCode == APIResponseCode.TRANSACTION_AUTHENTICATE {
                
                guard let redirectUrl:String = response.redirectUrl else { return }
                self.openAuthentication(redirectUrl: redirectUrl) //Open WebView for 3DS
            }
                
            else {
                
                //Get error response and display error
                print("\(type(of: self)) \(String(describing: response.responseCode)) :: \(String(describing: response.responseDescription))")
            }
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    
    override func inquiry(transactionID: String) {
        
        self.progressDialog.message = Constants.common_message_payment_result_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 5: Get payment result.
        self.merchantServerSimulator.inquiryPaymentResult(paymentInquiryRequest: self.buildPaymentInquiry(transactionID: transactionID), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            self.displayResult(response)
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
    }    
    
    func getChargeNextDate() -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "ddMMyyyy"
        
        let nextDate = Calendar.current.date(byAdding: Calendar.Component.day, value: 1, to: Date())
        
        return dateFormatter.string(from: nextDate!)
    }
    
    func setCardPayment( card:CreditCardPayment ){
        creditCardPayment = card
    }
    
    private func openAuthentication(redirectUrl:String) {
        
        let wkWebVC:WKWebViewController =  self.viewController.storyboard?.instantiateViewController(withIdentifier: "webview") as! WKWebViewController
        wkWebVC.redirectUrl = redirectUrl
        
        self.viewController.present(wkWebVC, animated: true, completion: nil)
        
    }
    
}
