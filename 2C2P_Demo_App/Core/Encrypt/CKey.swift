//
//  MerchantServer.swift
//  2C2P_Demo
//
//  Created by BGod MGod on 5/22/19.
//  Copyright © 2019 Macbook Pro. All rights reserved.
//

import Foundation

import Foundation
import PGW

class CKey {
    private var httpHelper:HTTPHelper
    static var CCKEY:String!
    static var MID:String!
    static var MSKEY:String!
    static var MID_2:String!
    static var MSKEY_2:String!
    
    static var MID_USED:String!
    static var MSKEY_USED:String!
    
    static var isReady:Bool!
    
    init() {
        CKey.isReady = false
        
        httpHelper = HTTPHelper()
        
        // Request SKEY
        httpHelper.requestkey(success: { (response:String) in
            
            CKey.CCKEY = response
            
            // Request Merchant For CC Payment
            self.httpHelper.requestMerchant(success: { (response:String) in
                let split = response.components(separatedBy: "||")
                CKey.MID   = split[0]
                CKey.MSKEY = split[1]
            
                // Request Merchant For Installment
                self.httpHelper.requestMerchant_Install(success: { (response:String) in
                    let split = response.components(separatedBy: "||")
                    CKey.MID_2   = split[0]
                    CKey.MSKEY_2 = split[1]
                                    
                    CKey.isReady = true
                    
                }) { (error:NSError) in
                    CKey.MID_2   = ""
                    CKey.MSKEY_2 = ""
                }
                
            }) { (error:NSError) in
                CKey.MID   = ""
                CKey.MSKEY = ""
            }
            
        }) { (error:NSError) in
            CKey.CCKEY = ""
        }
    }
    
    class func initPGW() {
        CKey.MID_USED   = CKey.MID
        CKey.MSKEY_USED = CKey.MSKEY
        
        // Start PGW
        PGWSDK.builder()
            .merchantID(CKey.MID_USED)
            .apiEnvironment(APIEnvironment.PRODUCTION)
            .initialize()
    }
    
    class func initPGW_Installment() {
        CKey.MID_USED   = CKey.MID_2
        CKey.MSKEY_USED = CKey.MSKEY_2
        
        // Start PGW
        PGWSDK.builder()
            .merchantID(CKey.MID_USED)
            .apiEnvironment(APIEnvironment.PRODUCTION)
            .initialize()
    }
}
