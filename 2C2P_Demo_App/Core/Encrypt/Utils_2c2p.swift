//
//  2c2pUtil.swift
//  2C2P_Demo
//
//  Created by BGod MGod on 5/24/19.
//  Copyright © 2019 Macbook Pro. All rights reserved.
//

import Foundation

class Utils_2c2p {
    
    class func convertTo_12_Digit(aData : String ) -> String {
        
        let digit = aData.replacingOccurrences(of: ".", with: "") ;
        
        let maxLenght = 12;
        let cLenght = digit.count;
        let loop = maxLenght - cLenght;
        var addString = "";
        
        for index in 1...loop {
            addString.append("0");
        }
        
        let finalString = String(format: "%@%@", addString,digit )
        //print("finalString ",finalString , finalString.count);
        return finalString;
    }
}
