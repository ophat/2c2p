//
//  Crypto.swift
//  2C2P_Demo
//
//  Created by BGod MGod on 5/13/19.
//  Copyright © 2019 Macbook Pro. All rights reserved.
//

import Foundation

class Crypto {
    
    class func gethash(string : String) -> String {
         
        let datr:Data = string.data(using: String.Encoding.utf8) as! Data
        let sha = self.sha256(data: datr as Data!)
        var hash = sha.base64EncodedString(options: []) as! String
        hash = hash.replacingOccurrences(of: "=", with: "")
        hash = hash.replacingOccurrences(of: "/", with: "")
        return hash
    }
    
    class func sha256(data : Data) -> Data {
        var hash = [UInt8](repeating: 0,  count: Int(CC_SHA256_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA256($0.baseAddress, CC_LONG(data.count), &hash)
        }
        return Data(hash)
    }
    
    class func encryptHashData (aData : String) -> String {
    
        let iv = AES256CBC.randomText(16);
        
        let encryptData = AES256CBC.encryptString(aData, password: CKey.CCKEY, aIv: iv)
        
        let hash = Crypto.gethash(string: encryptData!)
        let hashLenght = hash.count
        
        let keyhash = Crypto.gethash(string: CKey.CCKEY) as String
        let keyhashLenght = keyhash.count
        
        let data = String(format: "%@%d%@%@%d%@", hash , keyhashLenght , keyhash , encryptData! , hashLenght , iv )
        
        return data
    }
    
    class func decrypt(aData : String , aSKey : String , aCKey : String) -> String {
        let decrypted = AES256CBC.decryptString(aData, password: aSKey, aIv: aCKey)
        //print("decrypted ", decrypted)
        return decrypted!
    }
    
    class func decryptHashData (aData : String) -> String {
        
        //print("aData",aData)
        
        var startIndex = aData.count-16
        var endIndex = aData.count
        
        var start = String.Index(utf16Offset: startIndex, in: aData)
        var end = String.Index(utf16Offset: endIndex, in: aData)
        
        let iv = String(aData[start..<end])
        //print("iv",iv)
        
        startIndex = aData.count-18
        endIndex   = startIndex + 2
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)
        
        let hashLenght = String(aData[start..<end])
        //print("hashLenght ", hashLenght)
        
        startIndex = 0
        endIndex = Int(hashLenght)!
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)
        
        let hash = String(aData[start..<end])
        //print("hash ", hash)
        
        startIndex = Int(hashLenght)!
        endIndex = startIndex + 2
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)
        
        let skeylenght = String(aData[start..<end])
        //print("skeylenght ", skeylenght)
        
        startIndex = Int(hashLenght)! + 2
        endIndex = startIndex + Int(skeylenght)!
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)

        let skey = String(aData[start..<end])
        //print("skey ", skey)
        
        startIndex = Int(hashLenght)! + 2 + Int(skeylenght)!
        endIndex = aData.count - iv.count - 2
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)
        
        let data =  String(aData[start..<end])
        //print("data ", data)
        
        let validateData = self.gethash(string: data)
        let validateKey = self.gethash(string: CKey.CCKEY)
        
        // Validate Data & Key
        if hash != validateData {
            return ""
        }

        if skey != validateKey{
            return ""
        }
        
        let decrypted = AES256CBC.decryptString(data, password: CKey.CCKEY, aIv: iv)
        //print("decrypted ", decrypted)
        return decrypted!
    }
    
    class func decryptHashData2 (aData : String) -> String {
        //print("aData",aData)
        
        var startIndex = 16
        var endIndex = startIndex + 32
        
        var start = String.Index(utf16Offset: startIndex, in: aData)
        var end = String.Index(utf16Offset: endIndex, in: aData)
        
        let key = String(aData[start..<end])
        //print("key",key)
        
        startIndex = aData.count - 32 - 16
        endIndex = aData.count - 32
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)
        
        let iv = String(aData[start..<end])
        //print("iv",iv)
        
        startIndex = 48
        endIndex = aData.count - 32 - 16
        
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)
        
        let enData = String(aData[start..<end])
        //print("enData",enData)
        
        let decrypted = AES256CBC.decryptString(enData, password: key, aIv: iv)
        //print("decrypted ", decrypted!)
        
        return decrypted!
    
    }
  
    class func decryptHashData3 (aData : String) -> String {
        
        //print("aData",aData)
        
        var startIndex = aData.count - 32 - 16
        var endIndex = aData.count - 32
        
        var start = String.Index(utf16Offset: startIndex, in: aData)
        var end = String.Index(utf16Offset: endIndex, in: aData)
        
        let iv = String(aData[start..<end])
        //print("iv",iv)
        
        startIndex = 16
        endIndex = startIndex + 2
        
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)
        
        let en1_l = String(aData[start..<end])
        //print("en1_l",en1_l)
        
        startIndex = 16 + 2
        endIndex = startIndex + Int(en1_l)!;
        
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)
        
        let enData1 = String(aData[start..<end])
        //print("enData1",enData1)
        
        startIndex = 16 + 2 + Int(en1_l)!;
        endIndex = aData.count - 32 - 16
        
        start = String.Index(utf16Offset: startIndex, in: aData)
        end = String.Index(utf16Offset: endIndex, in: aData)
        
        let enData2 = String(aData[start..<end])
        //print("enData2",enData2)
        
        let decrypted1 = AES256CBC.decryptString(enData1, password: CKey.CCKEY, aIv: iv)
        //print("decrypted enData1", decrypted1!)
        
        let decrypted2 = AES256CBC.decryptString(enData2, password: CKey.CCKEY, aIv: iv)
        //print("decrypted enData2", decrypted2!)
        
        return String(format: "%@||%@",decrypted1!,decrypted2!)
    }
}
