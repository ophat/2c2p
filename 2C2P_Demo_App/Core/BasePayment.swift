//
//  BasePayment.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import UIKit
import PGW

class BasePayment:NSObject {
        
    func buildPaymentToken() -> PaymentTokenRequest {
        return PaymentTokenRequest()
    }

    func execute() {}
    
    func submit(paymentToken:String) {}
    
    func inquiry(transactionID:String) {}
    
    var merchantServerSimulator:MerchantServerSimulator
    var stringHelper:StringHelper
    var viewController:UIViewController
    var progressDialog:ProgressDialog
    
    init(viewController vc:UIViewController) {
        
        viewController = vc
        merchantServerSimulator = MerchantServerSimulator()
        stringHelper = StringHelper()
        
        progressDialog = ProgressDialog.shared
    }
    
    /**
     * Build for payment inquiry request
     *
     * @param transactionID
     * @return
     */
    func buildPaymentInquiry(transactionID:String) -> PaymentInquiryRequest {
        
        //Construct payment inquiry request
        let request:PaymentInquiryRequest = PaymentInquiryRequest()
        request.transactionID = transactionID
        
        return request
    }
    
    func displayResult(_ response:String) {
        
        guard let decodedRes:String = ToolKit.base64DecodedString(encodedString: response) else { return }
        
        guard let jsonObject:[String:Any] = ToolKit.dicFrom(jsonString: decodedRes) else { return }
        
        guard let responseCode:String = jsonObject[PGW.Constants.JSON_NAME_RESP_CODE] as? String else { return }
        
        let status : String;
        
        if(responseCode != APIResponseCode.API_SUCCESS) {
            
            status = "Fail!";
            
            let alert = UIAlertController(title: status , message: Constants.transaction_result_message_failed , preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            
            self.viewController.present(alert, animated: true);

        }else{
            status = "Success!";
            
        }
        // even fail need info any way
            
        let invoice     = jsonObject[PGW.Constants.JSON_NAME_INVOICE_NO] as? String
        let amount      = jsonObject[PGW.Constants.JSON_NAME_AMOUNT] as? String
        let dateTime    = jsonObject[Constants.JSON_NAME_TRANSACTION_DATE_TIME] as? String
        let paymentType = jsonObject[PGW.Constants.JSON_NAME_CHANNEL_CODE] as? String
        let maskedPan   = jsonObject[PGW.Constants.JSON_NAME_PAN] as? String
        let message     = jsonObject[PGW.Constants.JSON_NAME_RESP_DESC] as? String //  alway nil
        let responseMessageExpand = ToolKit.prettyPrintJsonString(jsonStr: decodedRes)
        var rs:String   = "";
        
        
        if invoice != nil {
            rs += "invoice : " + invoice! + "\n";
        }
        
        if amount != nil {
            rs += "amount : " + amount! + "\n";
        }
        
        if dateTime != nil {
            rs += "dateTime : " + dateTime! + "\n";
        }
        
        if paymentType != nil {
            rs += "paymentType : " + paymentType! + "\n";
        }
        
        if maskedPan != nil {
            rs += "maskedPan : " + maskedPan! + "\n";
        }
        
        if message != nil {
            rs += "message : " + message! + "\n";
        }
        
        if responseMessageExpand != nil {
            rs += "responseMessageExpand : " + responseMessageExpand;
        }
        
        print(rs);
        
        let alert = UIAlertController(title: status , message: rs , preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.viewController.present(alert, animated: true);
        
    }
}
