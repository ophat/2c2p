//
//  RetrievePaymentOptionDetails.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 6/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation
import PGW

class RetrievePaymentOptionDetails: BasePayment {
    
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-payment-options-details-api
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#section--paymentoptiondetailrequest-class-parameters-
     */
    
    override func buildPaymentToken() -> PaymentTokenRequest {
        
        //Construct payment token request
        let request:PaymentTokenRequest = PaymentTokenRequest()
        request.paymentChannel = "ALL"
        
        return request
    }
    
    
     func execute(aPayment:PaymentTemp) {
        if CKey.isReady {
            self.progressDialog.message = Constants.common_message_payment_token_message
            self.progressDialog.showLoadingIndicator()
            
            //Step 1 : Get payment token
            self.merchantServerSimulator.getPaymentToken(paymentTokenRequest: self.buildPaymentToken(),payment:aPayment, success: { (response:String) in
                
                self.progressDialog.hideLoadingIndicator()
                
                //Submit payment
                self.submit(paymentToken: self.stringHelper.extractPaymentToken(response: response))
            }) { (error:NSError) in
                
                self.progressDialog.hideLoadingIndicator()
                
                print("\(type(of: self)) \(error.domain)")
            }
        }
    }
    
    
    override func submit(paymentToken: String) {
        
        self.progressDialog.message = Constants.common_message_payment_processing_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 2: Construct payment option details request.
        let paymentOptionDetailRequest:PaymentOptionDetailRequest = PaymentOptionDetailRequest()
        paymentOptionDetailRequest.paymentToken = paymentToken
        paymentOptionDetailRequest.paymentChannel = PaymentChannel.CREDIT_CARD
        
        //Step 3: Retrieve payment option details.
        PGWSDK.shared.paymentOptionDetail(paymentOptionDetailRequest: paymentOptionDetailRequest, success: { (response:PaymentOptionDetailResponse) in
            
            self.progressDialog.hideLoadingIndicator()
            
            if response.responseCode == APIResponseCode.API_SUCCESS {
                
                guard let creditCardOptionDetail:CreditCardOptionDetail = response.creditCardOptionDetails else { return }
                self.validateCreditCard(creditCardOptionDetail: creditCardOptionDetail)
            } else {
                
                //Get error response and display error
                print("\(type(of: self)) \(String(describing: response.responseCode)) :: \(String(describing: response.responseDescription))")
            }
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        }
        
    }

    override func inquiry(transactionID: String) {
        //no required for this API
    }

    /**
     * Example for validate credit card input by using Retrieve payment option details API
     *
     * @param creditCardOptionDetail
     */
    private func validateCreditCard(creditCardOptionDetail:CreditCardOptionDetail) {
        
        let userInputCardNumber:String = "4111111111111111"
        
        stopLoop:
        for bin:String in creditCardOptionDetail.bins {
            
            //Do validate for supported card number
            if userInputCardNumber.starts(with: bin) {
                
                for cardType:CardType in creditCardOptionDetail.cardTypes {
                    
                    //Validate for card number length
                    if cardType.prefixes.contains(bin) {
                        
                        if userInputCardNumber.count < cardType.minLength
                           || userInputCardNumber.count > cardType.maxLength {
                            
                            //Invalid card number length
                            print("\(type(of: self)) ValidateCreditCard : Invalid.")
                        } else {
                            
                            //Valid credit card number
                            print("\(type(of: self)) ValidateCreditCard : Valid.")
                        }
                        
                        break stopLoop
                    }
                }
            }
        }
    }
}
