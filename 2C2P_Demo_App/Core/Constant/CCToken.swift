//
//  CCToken.swift
//  2C2P_Demo
//
//  Created by BGod MGod on 5/14/19.
//  Copyright © 2019 Macbook Pro. All rights reserved.
//

import Foundation
class CCToken {
    
    static var share:CCToken! = nil
    var ccToken:String!
    
    class func shareInstace() ->Any!{
        if share != nil{
            return share
        }
        return nil
    }
    
    init() {
        CCToken.share = self
    }
    
}
