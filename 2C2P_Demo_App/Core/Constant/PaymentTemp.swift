//
//  PaymentTemp.swift
//  2C2P_Demo
//
//  Created by BGod MGod on 5/9/19.
//  Copyright © 2019 Macbook Pro. All rights reserved.
//

import Foundation

class PaymentTemp {
    
    var payment_desc:String!
    var payment_amount:String! // must be 12 digit
    var payment_currencyType:String! // must be 3 alphabet
    var payment_productCode:String!
 
    var payment_Installment_Period:Int!
    
    var payment_recurring:String!
    var payment_invoice_prefix:String!
    var payment_recurring_amount:String!  // must be 12 digit
    var payment_allow_accumulate:String!  // Y/N
    var payment_max_accumulateAmt:String! // must be 12 digit
    var payment_recurring_interval:String!
    var payment_recurring_count:String!
    var payment_charge_next_date:String!  // DDMMYYYY
    
    var payment_user_define_1:String!
    var payment_user_define_2:String!
    var payment_user_define_3:String!
    var payment_user_define_4:String!
    var payment_user_define_5:String!   
    
//    $recurring = "Y";                //Enable / Disable RPP option
//    $invoice_prefix = 'demo'.time();            //RPP transaction invoice prefix
//    $recurring_amount = "000000000100";        //Recurring amount
//    $allow_accumulate = "Y";                //Allow failed authorization to be accumulated
//    $max_accumulateAmt = "000000000100";                //Maximum threshold of total accumulated amount
//    $recurring_interval = "5";            //Recurring interval by no of days
//    $recurring_count = "3";                //Number of Recurring occurance
//    $charge_next_date = (new DateTime('tomorrow'))->format("dmY");    //The first day to start recurring charges. format DDMMYYYY

    
}
