//
//  Constants.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 2/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

class Constants {
    
    static let HTTP_MERCHANT_SERVER_URL_SANDBOX                 = "https://backend.yuemmai.com/merchant_server/"
    static let HTTP_MERCHANT_SERVER_URL_PRODUCTION_INDONESIA    = "https://backend.yuemmai.com/merchant_server/"
    static let HTTP_MERCHANT_SERVER_URL_PRODUCTION              = "https://backend.yuemmai.com/merchant_server/"
    
    static let HTTP_MERCHANT_SERVER_PAYMENT_TOKEN_URL   = "/paymentToken/"
    static let HTTP_MERCHANT_SERVER_PAYMENT_INQUIRY_URL = "/paymentInquiry/"
    static let HTTP_MERCHANT_SERVER_KEY                 = "/key/"
    static let HTTP_MERCHANT_SERVER_MERCHANT            = "/mid/"
    static let HTTP_MERCHANT_SERVER_MERCHANT_2          = "/mid2/"
    
    static let HTTP_CONNECTION_TIMEOUT:Double           = 60
    
    //ERROR
    static let MESSAGE_ERROR_EMPTY_RESPONSE = "Empty response"
    static let MESSAGE_ERROR_CONVERT_FROM_JSON_FAILED = "Convert from json failed"
    static let MESSAGE_ERROR_PARAMETER_ERROR = "Parameter error"
    static let MESSAGE_ERROR_GET_PAYMENT_TOKEN_FAILED = "Get payment token failed."
    static let MESSAGE_ERROR_GET_PAYMENT_INQUIRY_FAILED = "Get payment inquiry failed."
    static let MESSAGE_ERROR_CONVERT_FROM_STRING_TO_DATA_FAILED = "Convert from type of String to Data failed"
    
    //JSON KEY NAME
    static let JSON_NAME_TRANSACTION_DATE_TIME = "transactionDateTime"
    
    static let JSON_NAME_PAYMENT_TOKEN = "paymentToken"
    
    //Common Message
    static let common_message_progress_loading_message = "Please wait, loading..."
    static let common_message_payment_processing_message = "Please wait, payment processing..."
    static let common_message_payment_result_message = "Please wait, getting payment result..."
    static let common_message_payment_token_message = "Please wait, getting payment token..."
    
    //Transaction result
    static let transaction_result_header_success = "Your payment success!"
    static let transaction_result_header_failed = "Your payment failed!"
    static let transaction_result_message_success = "Your order has been placed! \n We\'ll reach out to you shortly with your order."
    static let transaction_result_message_failed = "Please re-order product again or call to our support team."
    static let transaction_result_order_detail_title = "ORDER DETAILS"
    static let transaction_result_invoice_no_title = "Invoice No. :"
    static let transaction_result_date_time_title = "DateTime :"
    static let transaction_result_amount_title = "Amount :"
    static let transaction_result_payment_type_title = "Payment Type :"
    static let transaction_result_masked_pan_title = "Credit Card No. :"
    static let transaction_result_status_title = "Status :"
    static let transaction_result_expand_title = "Full Payment Response :"
    static let transaction_result_title = "Payment result"
}
