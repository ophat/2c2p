//
//  ProgressDialog.swift
//  PGW4
//
//  Created by yan feng liu on 31/7/18.
//  Copyright © 2018 My2c2p. All rights reserved.
//

import UIKit

class ProgressDialog {
    
    //singleton definition
    static let shared:ProgressDialog = ProgressDialog()
    
    private var loadingBGView:UIView
    private var loadingView:UIView
    private var loadingIndicator:UIActivityIndicatorView
    private var loadingLabel:UILabel
    
    private let gap:CGFloat = 10
    
    var message:String? {
        get {
            return self.loadingLabel.text
        }
        set {
            self.loadingLabel.text = newValue
        }
    }
    
    init() {
        
        let loadingIndicatorFrame:CGRect = CGRect(x: 0, y: 0, width: 80, height: 80)
        
        //loadingIndicator
        loadingIndicator = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.gray)
        loadingIndicator.frame = loadingIndicatorFrame
        
        //loadingLabel
        let loadingLabelFrame:CGRect = CGRect(x: loadingIndicatorFrame.maxX, y: 0, width: UIScreen.main.bounds.size.width - 160, height: 80)
        
        loadingLabel = UILabel(frame: loadingLabelFrame)
        loadingLabel.text = Constants.common_message_progress_loading_message
        loadingLabel.textColor = UIColor.darkGray
        loadingLabel.textAlignment = NSTextAlignment.left
        
        //loadingView
        let loadingViewFrame:CGRect = CGRect(x: gap * 2, y: UIScreen.main.bounds.size.height / 2.0, width: UIScreen.main.bounds.size.width - gap * 4, height: 80)
        
        loadingView = UIView(frame: loadingViewFrame)
        loadingView.backgroundColor = UIColor.white
        loadingView.addSubview(loadingIndicator)
        loadingView.addSubview(loadingLabel)
        
        //loadingBGView
        loadingBGView = UIView(frame: UIScreen.main.bounds)
        loadingBGView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        loadingBGView.isHidden = true
        loadingBGView.addSubview(loadingView)
        
        guard let window:UIWindow = UIApplication.shared.keyWindow else {
            return
        }

        window.addSubview(loadingBGView)
    }
    
    func showLoadingIndicator() {
        
        self.loadingIndicator.startAnimating()
        self.loadingBGView.isHidden = false
    }
    
    func hideLoadingIndicator() {
        if self.loadingIndicator.isAnimating {
            self.loadingIndicator.stopAnimating()
            self.loadingBGView.isHidden = true
        }
    }
    
}
