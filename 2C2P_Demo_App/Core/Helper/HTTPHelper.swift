//
//  HTTPHelper.swift
//  PGW4
//
//  Created by yan feng liu on 27/7/18.
//  Copyright © 2018 My2c2p. All rights reserved.
//

import Foundation
import PGW

class HTTPHelper {
    
    func requestMerchant(success:@escaping response, failure:@escaping failure) -> Void {
        
        guard let reqURL:URL = URL.init(string: self.getServerPath() + Constants.HTTP_MERCHANT_SERVER_MERCHANT ) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_GET_PAYMENT_TOKEN_FAILED, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        var request:URLRequest = URLRequest.init(url: reqURL, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: Constants.HTTP_CONNECTION_TIMEOUT)
        
        request.httpMethod = "POST"
        
        NetworkManager.shared.request(request: request) { (error:NSError?, response) in
            if error == nil {
                //                Decrypt Data HERE
                let data = Crypto.decryptHashData3(aData: response)
                //print("requestkey Response",data)
                success(data)
            } else {
                failure(error!)
            }
        }
    }
    
    func requestMerchant_Install(success:@escaping response, failure:@escaping failure) -> Void {
        
        guard let reqURL:URL = URL.init(string: self.getServerPath() + Constants.HTTP_MERCHANT_SERVER_MERCHANT_2 ) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_GET_PAYMENT_TOKEN_FAILED, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        var request:URLRequest = URLRequest.init(url: reqURL, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: Constants.HTTP_CONNECTION_TIMEOUT)
        
        request.httpMethod = "POST"
        
        NetworkManager.shared.request(request: request) { (error:NSError?, response) in
            if error == nil {
                //                Decrypt Data HERE
                let data = Crypto.decryptHashData3(aData: response)
                success(data)
            } else {
                failure(error!)
            }
        }
    }
    
    func requestkey(success:@escaping response, failure:@escaping failure) -> Void {
        
        guard let reqURL:URL = URL.init(string: self.getServerPath() + Constants.HTTP_MERCHANT_SERVER_KEY ) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_GET_PAYMENT_TOKEN_FAILED, code: -1, userInfo: nil)
            failure(error)
            
            return
        }

        var request:URLRequest = URLRequest.init(url: reqURL, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: Constants.HTTP_CONNECTION_TIMEOUT)
        
        request.httpMethod = "POST"
        
        NetworkManager.shared.request(request: request) { (error:NSError?, response) in
            if error == nil {
//                Decrypt Data HERE
                let data = Crypto.decryptHashData2(aData: response)
                //print("requestkey Response",data)
                success(data)
            } else {
                failure(error!)
            }
        }
    }
    
    func requestPaymentToken(encodedJson:String, success:@escaping response, failure:@escaping failure) -> Void {
        
        guard let reqURL:URL = URL.init(string: self.getServerPath() + Constants.HTTP_MERCHANT_SERVER_PAYMENT_TOKEN_URL) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_GET_PAYMENT_TOKEN_FAILED, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        var request:URLRequest = URLRequest.init(url: reqURL, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: Constants.HTTP_CONNECTION_TIMEOUT)
        
        request.httpMethod = "POST"

        guard let encodedJsonData:Data = encodedJson.data(using: String.Encoding.utf8) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_CONVERT_FROM_STRING_TO_DATA_FAILED, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        request.httpBody = encodedJsonData
        
        NetworkManager.shared.request(request: request) { (error:NSError?, response) in
            if error == nil {
                //Decrypt Data HERE
                let data = Crypto.decryptHashData(aData: response)
                //print("PaymentToken Response",data)
                success(data)
            } else {
                //print("response",response)
                failure(error!)
            }
        }
    }
    
    func requestPaymentInquiry(encodedJson:String, success:@escaping response, failure:@escaping failure) {
        
        guard let reqURL:URL = URL.init(string: self.getServerPath() + Constants.HTTP_MERCHANT_SERVER_PAYMENT_INQUIRY_URL) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_GET_PAYMENT_INQUIRY_FAILED, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        var request:URLRequest = URLRequest.init(url: reqURL, cachePolicy: URLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: Constants.HTTP_CONNECTION_TIMEOUT)
        
        request.httpMethod = "POST"
        
        guard let encodedJsonData:Data = encodedJson.data(using: String.Encoding.utf8) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_CONVERT_FROM_STRING_TO_DATA_FAILED, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
         
        request.httpBody = encodedJsonData
        
        NetworkManager.shared.request(request: request) { (error:NSError?, response) in
            if error == nil {
                //Decrypt Data HERE
                let data = Crypto.decryptHashData(aData: response)
                //print("PaymentInquiry Response",data)
                success(data)
            } else {
                //print("response",response)
                failure(error!)
            }
        }
        
    }
    
    private func getServerPath() -> String {
        
        let apiEnvironment:APIEnvironment = PGWSDK.shared.apiEnvironment
        
        if apiEnvironment == APIEnvironment.SANDBOX {
            return Constants.HTTP_MERCHANT_SERVER_URL_SANDBOX
        } else if apiEnvironment == APIEnvironment.PRODUCTION_INDONESIA {
            return Constants.HTTP_MERCHANT_SERVER_URL_PRODUCTION_INDONESIA
        } else {
            return Constants.HTTP_MERCHANT_SERVER_URL_PRODUCTION
        }
    }
    
}
