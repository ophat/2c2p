//
//  NetworkManager.swift
//  PGWSDK
//
//  Created by yan feng liu on 7/6/18.
//  Copyright © 2018 My2c2p. All rights reserved.
//

import UIKit

typealias NetworkManagerCallback = (_ err:NSError?,_ response:String) -> ()

class NetworkManager:NSObject {
    //singleton definition
    static var shared:NetworkManager = NetworkManager()
    
    func request(request:URLRequest, callback:@escaping NetworkManagerCallback) {
        DispatchQueue.global(qos: DispatchQoS.QoSClass.default).async {
            let (err,response) = self.doRequest(request: request)
            
            DispatchQueue.main.async {
                callback(err, response)
            }
            
        }
    }
    
    
    private func doRequest(request:URLRequest) -> (NSError?, String) {
        
        var myResponse:String = String()
        var err:NSError?
        
        let semaphore = DispatchSemaphore.init(value: 0)    //create signal
        let session = URLSession.init(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)
        let task = session.dataTask(with: request) { data, response, error in
            if error != nil {
                //error   
                err = error as NSError?
                semaphore.signal()    //send signal
                return
            }
            
            if data == nil || data!.isEmpty {
                print("request",request,response)
                //error
                err = NSError.init(domain: Constants.MESSAGE_ERROR_EMPTY_RESPONSE, code: -1, userInfo: nil)
                semaphore.signal()    //send signal
                return
            }
            
            guard let res:String = String(data: data!, encoding: String.Encoding.utf8) else {
                //error
                err = NSError.init(domain: Constants.MESSAGE_ERROR_CONVERT_FROM_JSON_FAILED, code: -1, userInfo: nil)
                semaphore.signal()    //send signal
                return
            }
            
            myResponse = res
            semaphore.signal()    //send signal
        }
        
        task.resume()
        _ = semaphore.wait(timeout: DispatchTime.distantFuture)   // wait signal
        
        return (err, myResponse)
    }
    
}

//URLSession Delegate
extension NetworkManager:URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(URLSession.AuthChallengeDisposition.useCredential, URLCredential(trust: challenge.protectionSpace.serverTrust!))

    }
}

