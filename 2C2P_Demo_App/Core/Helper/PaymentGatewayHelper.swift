//
//  PaymentGatewayHelper.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 4/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

class PaymentGatewayHelper {
    
    func generateSignature(secretKey:String, jsonString:String) -> String {
                
        let stringArrayList:[String] = self.jsonToArray(jsonString: jsonString)
        let sortedStringArrayList:[String] = self.sortArray(list: stringArrayList)
        let arrayString:String = self.arrayToString(list: sortedStringArrayList)
        
        return self.hashSignature(secretKey: secretKey, arrayString: arrayString)
    }
    
    private func jsonToArray(jsonString:String) -> [String] {
        
        var list:[String] = [String]()
        guard let object:[String:Any] = ToolKit.dicFrom(jsonString: jsonString) else {return list}
        
        for key in object.keys {
            list.append(object[key] as! String)
        }
        
        return list
    }
    
    private func sortArray(list:[String]) -> [String] {
        
        return list.sorted { (key1:String, key2:String) -> Bool in
            if key1.caseInsensitiveCompare(key2) == ComparisonResult.orderedDescending {
                return false
            } else {
                return true
            }
        }
        
    }
    
    private func arrayToString(list:[String]) -> String {
        
        var arrayString:String = ""
        
        for value in list {
            arrayString += value
        }
        
        return arrayString
    }
    
    
    private func hashSignature(secretKey:String, arrayString:String) -> String {
        return arrayString.hmac(algorithm: CCHmacAlgorithm(kCCHmacAlgSHA256), key: secretKey)
    }
}

//MARK: - HMAC
extension String {
    
    func hmac(algorithm:CCHmacAlgorithm, key: String) -> String {
        
        let cKey = key.cString(using: String.Encoding.utf8)
        let cData = self.cString(using: String.Encoding.utf8)
        var result = [CUnsignedChar](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        CCHmac(algorithm, cKey, strlen(cKey!), cData, strlen(cData!), &result)
        
        let digest = self.stringFromResult(result: &result, length: Int(CC_SHA256_DIGEST_LENGTH))
        
        return digest
    }
    
    private func stringFromResult(result: UnsafeMutablePointer<CUnsignedChar>, length: Int) -> String {
        
        let hash = NSMutableString()
        for i in 0..<length {
            hash.appendFormat("%02x", result[i])
        }
        return String(hash)
    }
}

