//
//  MerchantServerSimulator.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

class MerchantServerSimulator {
    
    /**
     * IMPORTANT : MerchantServerSimulator just for simulator how merchant server work on demo app, and DO NOT call payment token API directly from your application.
     */
    
    private var httpHelper:HTTPHelper
    private var paymentGatewayHelper:PaymentGatewayHelper
    private var stringHelper:StringHelper
    
    init() {
        
        httpHelper = HTTPHelper()
        paymentGatewayHelper = PaymentGatewayHelper()
        stringHelper = StringHelper()
    }
    
    /**
     * @param paymentTokenRequest
     * @param success
     * @param failure
     *
     * Reference : https://developer.2c2p.com/docs/mobile-v4-how-to-integrate
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#section--payment-token-request-parameters-
     */
    func getPaymentToken(paymentTokenRequest:PaymentTokenRequest, payment:PaymentTemp, success:@escaping response, failure:@escaping failure) {
        
        //Request information
        let apiVersion:String = "10.0"
        let nonceStr:String = UUID().uuidString
        
        //Merchant's account information
        let mid:String = CKey.MID_USED
        let secretKey:String = CKey.MSKEY_USED
        
        //Transaction information
        let invoiceNo:String = String(Int(Date().timeIntervalSince1970))
        let desc:String = payment.payment_desc
        let amount:String = payment.payment_amount
        let currencyCode:String = payment.payment_currencyType
        let productCode:String = payment.payment_productCode
        
        //Set common values for all payments
        paymentTokenRequest.version = apiVersion
        paymentTokenRequest.nonceStr = nonceStr
        paymentTokenRequest.merchantID = mid
        paymentTokenRequest.desc = desc
        paymentTokenRequest.invoiceNo = invoiceNo
        paymentTokenRequest.currencyCode = currencyCode
        paymentTokenRequest.amount = amount
        paymentTokenRequest.productCode = productCode
        
        // recurring
        paymentTokenRequest.recurring = payment.payment_recurring
        paymentTokenRequest.invoicePrefix = payment.payment_invoice_prefix
        paymentTokenRequest.recurringAmount = payment.payment_recurring_amount
        paymentTokenRequest.allowAccumulate = payment.payment_allow_accumulate
        paymentTokenRequest.maxAccumulateAmt = payment.payment_max_accumulateAmt
        paymentTokenRequest.recurringInterval = payment.payment_recurring_interval
        paymentTokenRequest.recurringCount = payment.payment_recurring_count
        paymentTokenRequest.chargeNextDate = payment.payment_charge_next_date
       
        paymentTokenRequest.userDefined1 = payment.payment_user_define_1
        paymentTokenRequest.userDefined2 = payment.payment_user_define_2
        paymentTokenRequest.userDefined3 = payment.payment_user_define_3
        paymentTokenRequest.userDefined4 = payment.payment_user_define_4
        paymentTokenRequest.userDefined5 = payment.payment_user_define_5
        
        //Generate signature
        guard let paymentTokenRequestJson:String = ToolKit.jsonStringFrom(dic: paymentTokenRequest.toJSON()) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        paymentTokenRequest.signature = self.paymentGatewayHelper.generateSignature(secretKey: secretKey, jsonString: paymentTokenRequestJson)
        
        guard let finalPaymentTokenRequestJson:String = ToolKit.jsonStringFrom(dic: paymentTokenRequest.toJSON()) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        //print("getPaymentToken JSON: \(finalPaymentTokenRequestJson)")
        
        guard let encodedPaymentTokenRequestJson:String = self.stringHelper.base64Encode(input: finalPaymentTokenRequestJson) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        //print("encodedPaymentTokenRequestJson: \(encodedPaymentTokenRequestJson)")
        
        ///==============================================================================
    
        let data = Crypto.encryptHashData(aData: encodedPaymentTokenRequestJson)
        //print("final PaymentToken : \(data)")
        self.httpHelper.requestPaymentToken(encodedJson: data, success: success, failure: failure)
    
    }
    
    /**
     * @param paymentInquiryRequest
     * @param success
     * @param failure
     *
     * Reference : https://developer.2c2p.com/docs/mobile-v4-payment-inquiry-api
     *             https://developer.2c2p.com/docs/mobile-v4-api-parameters#section--payment-inquiry-response-parameters-
     */
    func inquiryPaymentResult(paymentInquiryRequest:PaymentInquiryRequest , success:@escaping response, failure:@escaping failure) {
        
        //Request information
        let apiVersion:String = "1.1"
        
        //Merchant's account information
        let mid:String = CKey.MID_USED
        let secretKey:String = CKey.MSKEY_USED
        
        //Set common values for all payments
        paymentInquiryRequest.version = apiVersion
        paymentInquiryRequest.merchantID = mid
        
        //Generate signature
        guard let paymentInquiryRequestJson:String = ToolKit.jsonStringFrom(dic: paymentInquiryRequest.toJSON()) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        paymentInquiryRequest.signature = self.paymentGatewayHelper.generateSignature(secretKey: secretKey, jsonString: paymentInquiryRequestJson)
        
        guard let finalPaymentInquiryRequestJson:String = ToolKit.jsonStringFrom(dic: paymentInquiryRequest.toJSON()) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        //print("inquiryPaymentResult JSON: \(finalPaymentInquiryRequestJson)")
        
        guard let encodedPaymentInquiryRequestJson:String = self.stringHelper.base64Encode(input: finalPaymentInquiryRequestJson) else {
            
            let error:NSError = NSError.init(domain: Constants.MESSAGE_ERROR_PARAMETER_ERROR, code: -1, userInfo: nil)
            failure(error)
            
            return
        }
        
        //print("encodedPaymentInquiryRequestJson JSON: \(encodedPaymentInquiryRequestJson)")
        
        //==============================================================================
 
        let data = Crypto.encryptHashData(aData: encodedPaymentInquiryRequestJson)
        //print("final PaymentInquiry : \(data)")
        self.httpHelper.requestPaymentInquiry(encodedJson: data, success: success, failure: failure)
        
    }
    
}


