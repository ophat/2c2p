//
//  StringHelper.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

class StringHelper {
    
    func base64Encode(input:String) -> String? {
        
        guard let plainData:Data = input.data(using: String.Encoding.utf8) else { return nil }
        
        let base64Str:String = plainData.base64EncodedString(options: Data.Base64EncodingOptions(rawValue: 0))
        
        return base64Str
    }
    
    func extractPaymentToken(response:String) -> String {
        
        guard let decodedRes:String = ToolKit.base64DecodedString(encodedString: response) else { return "" }
        
        guard let jsonObject = ToolKit.dicFrom(jsonString: decodedRes) else { return "" }
        
        guard let paymentToken:String = jsonObject[Constants.JSON_NAME_PAYMENT_TOKEN] as? String else {
            return ""
        }
        
        return paymentToken
    }

}
