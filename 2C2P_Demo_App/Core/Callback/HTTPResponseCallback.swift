//
//  HTTPResponseCallback.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 2/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

public typealias response = (_ response:String) -> ()
public typealias failure = (_ error:NSError) -> ()
