//
//  PaymentInquiryRequest.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 3/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import Foundation

class PaymentInquiryRequest {
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-api-parameters#section--payment-inquiry-request-parameters-
     * Version : 1.1
     * {
     *    "version": "1.1",
     *    "merchantID": "",
     *    "transactionID": "",
     *    "invoiceNo": "",
     *    "signature": ""
     * }
     */
    
    var version:String?
    var merchantID:String?
    var invoiceNo:String?
    var transactionID:String?
    var signature:String?
    
    func toJSON() -> [String:String] {
        var json:[String:String]
        
        json = [
            "version"        : JSONHelper.option(input: self.version, defaultValue: ""),
            "merchantID"     : JSONHelper.option(input: self.merchantID, defaultValue: ""),
            "transactionID"  : JSONHelper.option(input: self.transactionID, defaultValue: ""),
            "invoiceNo"      : JSONHelper.option(input: self.invoiceNo, defaultValue: ""),
            "signature"      : JSONHelper.option(input: self.signature, defaultValue: "")
        ]
        
        return json
    }
}
