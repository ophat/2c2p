//
//  WKWebViewController.swift
//  Merchant_Demo
//
//  Created by yan feng liu on 5/10/18.
//  Copyright © 2018 2C2P. All rights reserved.
//

import UIKit
import WebKit
import PGW

class WKWebViewController: UIViewController {
    
    /**
     * Reference : https://developer.2c2p.com/docs/mobile-v4-3ds-or-non-3ds#step-5
     */
    
    //var webView:WKWebView!
    var pgwWebViewDelegate:PGWWKWebViewDelegate!
    var redirectUrl:String?
    
    @IBOutlet weak var uView: UIView!
    var webViewC: WKWebView!
    
    private var merchantServerSimulator:MerchantServerSimulator!
    private var progressDialog:ProgressDialog!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.merchantServerSimulator = MerchantServerSimulator()
        self.progressDialog = ProgressDialog.shared
    }
    
    override func viewDidLayoutSubviews() {
        let requestUrl:URL = URL.init(string: self.redirectUrl!)!
        let request:URLRequest = URLRequest.init(url: requestUrl)
        
        let jScript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript.init(source: jScript, injectionTime: WKUserScriptInjectionTime.atDocumentEnd, forMainFrameOnly: true)
        let wkController = WKUserContentController.init()
        wkController.addUserScript(userScript)
        
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.userContentController = wkController
        
        self.webViewC = WKWebView(frame: uView.bounds, configuration: webConfiguration)
        self.webViewC.navigationDelegate = self.transactionResultCallback()
        self.webViewC.load(request)
        
        uView.addSubview(webViewC)
    }
    
    func transactionResultCallback() -> PGWWKWebViewDelegate {
        
        //print("transactionResultCallback")
        
        self.pgwWebViewDelegate = PGWWKWebViewDelegate(success: { (response: TransactionResultResponse) in
            
            if response.responseCode == APIResponseCode.TRANSACTION_COMPLETED {
                
                //Inquiry payment result by using transaction id.
                self.inquiry(transactionID: response.transactionID!)
                
            } else {
                
                //Get error response and display error
                print("\(type(of: self)) \(String(describing: response.responseCode)) :: \(String(describing: response.responseDescription))")
            }
        }, failure: { (error: NSError) in
            
            //Get error response and display error
            print("\(type(of: self)) \(error.domain)")
        })
        
        return self.pgwWebViewDelegate
    }
    
    /**
     * Build for payment inquiry request
     *
     * @param transactionID
     * @return
     */
    func buildPaymentInquiry(transactionID:String) -> PaymentInquiryRequest {
        
        //Construct payment inquiry request
        let request:PaymentInquiryRequest = PaymentInquiryRequest()
        request.transactionID = transactionID
        
        return request
    }
    
    /**
     * For Non-3DS
     * @param transactionID
     */
    func inquiry(transactionID: String) {
        
        self.progressDialog.message = Constants.common_message_payment_result_message
        self.progressDialog.showLoadingIndicator()
        
        //Step 6: Get payment result.
        self.merchantServerSimulator.inquiryPaymentResult(paymentInquiryRequest: self.buildPaymentInquiry(transactionID: transactionID), success: { (response:String) in
            
            self.progressDialog.hideLoadingIndicator()
            
            self.displayResult(response)
            
        }) { (error:NSError) in
            
            self.progressDialog.hideLoadingIndicator()
            
            print("\(type(of: self)) \(error.domain)")
        }
    }
    
    func displayResult(_ response:String) {
        
        guard let decodedRes:String = ToolKit.base64DecodedString(encodedString: response) else { return }
        
        guard let jsonObject:[String:Any] = ToolKit.dicFrom(jsonString: decodedRes) else { return }
        
        guard let responseCode:String = jsonObject[PGW.Constants.JSON_NAME_RESP_CODE] as? String else { return }
    
        let status : String;
        
        if(responseCode != APIResponseCode.API_SUCCESS) {
            
            status = "Fail!";
            
        }else{
            status = "Success!";
            
        }
        // even fail need info any way
        
        let invoice     = jsonObject[PGW.Constants.JSON_NAME_INVOICE_NO] as? String
        let amount      = jsonObject[PGW.Constants.JSON_NAME_AMOUNT] as? String
        let dateTime    = jsonObject[Constants.JSON_NAME_TRANSACTION_DATE_TIME] as? String
        let paymentType = jsonObject[PGW.Constants.JSON_NAME_CHANNEL_CODE] as? String
        let maskedPan   = jsonObject[PGW.Constants.JSON_NAME_PAN] as? String
        let message     = jsonObject[PGW.Constants.JSON_NAME_RESP_DESC] as? String //  alway nil
        let responseMessageExpand = ToolKit.prettyPrintJsonString(jsonStr: decodedRes)
        
        let cctoken = jsonObject[PGW.Constants.JSON_NAME_CARD_TOKEN] as? String //  alway nil
        
        
        CCToken.share.ccToken = cctoken
        
        print(CCToken.share.ccToken)
        
        var rs:String   = "";
        
        
        if invoice != nil {
            rs += "invoice : " + invoice! + "\n";
        }
        
        if amount != nil {
            rs += "amount : " + amount! + "\n";
        }
        
        if dateTime != nil {
            rs += "dateTime : " + dateTime! + "\n";
        }
        
        if paymentType != nil {
            rs += "paymentType : " + paymentType! + "\n";
        }
        
        if maskedPan != nil {
            rs += "maskedPan : " + maskedPan! + "\n";
        }
        
        if message != nil {
            rs += "message : " + message! + "\n";
        }
        
        if responseMessageExpand != nil {
            rs += "responseMessageExpand : " + responseMessageExpand;
        }
        
        print(rs);
        
        let alert = UIAlertController(title: status , message: rs , preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true);
        
    }
    
    @IBAction func Back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil);
    }
    
    
}
